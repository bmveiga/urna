const cors = require('cors')
const express  = require("express")
const fs = require("fs")

const app = express()

app.use(express.urlencoded({extended: false}))
app.use(express.json())
app.use(cors())

app.listen(3001, function (){
    console.log("SERVER IS UP");
})

app.get("/cargainicial", function(req, resp){
    
    fs.readFile('config.csv', 'utf8' , function(err, data){
        if (err){
            console.log("Erro ao ler o arquivo" + err);
        } else{

        }
        console.log(data);

        var eleicoes = data.split("\n")
        console.log(eleicoes);

        let candidatos = []

        for (let i = 0; i < eleicoes.length; i++) {
            const element = eleicoes[i];
            candidatos.push(element.split(","))
        }

        resp.send(candidatos)
        console.log(candidatos)
    })
})

